#include "framework.h"

int OnUndoEdit( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, WM_UNDO, 0, 0 );
	return 0;
}

int OnRedoEdit( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_REDO, 0, 0 );
	return 0;
}

int OnCutText( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, WM_CUT, 0, 0 );
	return 0;
}

int OnCopyText( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, WM_COPY, 0, 0 );
	return 0;
}

int OnPasteText( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, WM_PASTE, 0, 0 );
	return 0;
}

int OnDeleteText( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, WM_CLEAR, 0, 0 );
	return 0;
}

int OnCutLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINECUT, 0, 0 );
	return 0;
}

int OnCutLineAndPasteLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
		OnCutLine( pnodeTabPage );
		OnPasteLine( pnodeTabPage );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
	}
	return 0;
}

int OnCopyLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINECOPY, 0, 0 );
	return 0;
}

int OnCopyLineAndPasteLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SELECTIONDUPLICATE, 0, 0 );
	return 0;
}

int OnCopyFilename( struct TabPage *pnodeTabPage )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(pnodeTabPage->acFilename)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		ErrorBox( "不能分配内存用以复制文件名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , pnodeTabPage->acFilename );
	GlobalUnlock(hgl);

	::OpenClipboard( g_hwndMainWindow );
	::EmptyClipboard();
	::SetClipboardData( CF_TEXT , acCopyText );
	::CloseClipboard();

	return 0;
}

int OnCopyPathname( struct TabPage *pnodeTabPage )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(pnodeTabPage->acPathName)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		ErrorBox( "不能分配内存用以复制路径名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , pnodeTabPage->acPathName );
	GlobalUnlock(hgl);

	::OpenClipboard( g_hwndMainWindow );
	::EmptyClipboard();
	::SetClipboardData( CF_TEXT , acCopyText );
	::CloseClipboard();

	return 0;
}

int OnCopyPathFilename( struct TabPage *pnodeTabPage )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(pnodeTabPage->acPathFilename)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		ErrorBox( "不能分配内存用以复制路径文件名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , pnodeTabPage->acPathFilename );
	GlobalUnlock(hgl);

	::OpenClipboard( g_hwndMainWindow );
	::EmptyClipboard();
	::SetClipboardData( CF_TEXT , acCopyText );
	::CloseClipboard();

	return 0;
}

int OnPasteLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nNextLineStartPos ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		nNextLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nNextLineStartPos, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_PASTE, 0, 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nNextLineStartPos, 0 ) ;
	}

	return 0;
}

int OnPasteLineUpstairs( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nCurrentLineStartPos ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		nCurrentLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nCurrentLineStartPos, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_PASTE, 0, 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nCurrentLineStartPos, 0 ) ;
	}

	return 0;
}

int OnDeleteLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEDELETE, 0, 0 );
	return 0;
}

int OnDeleteWhiteCharacterAtLineHead( struct TabPage *pnodeTabPage )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( pnodeTabPage == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( pnodeTabPage , & nSelectStartLine , & nSelectEndLine );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos; 
		tr.lpstrText = acLineBuffer; 
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nDeleteLength] == ' ' || acLineBuffer[nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength > 0 )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, nLineStartPos, nDeleteLength );
	}
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int OnDeleteWhiteCharacterAtLineTail( struct TabPage *pnodeTabPage )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( pnodeTabPage == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( pnodeTabPage , & nSelectStartLine , & nSelectEndLine );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos; 
		tr.lpstrText = acLineBuffer; 
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nLineStringLength-1-nDeleteLength] == ' ' || acLineBuffer[nLineStringLength-1-nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength > 0 )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, nLineEndPos-nDeleteLength, nDeleteLength );
	}
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int OnDeleteBlankLine( struct TabPage *pnodeTabPage )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength , nLineLength ;

	if( pnodeTabPage == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( pnodeTabPage , & nSelectStartLine , & nSelectEndLine );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		nLineLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINELENGTH, nLine, 0 ) ;
		if( nLineStringLength == 0 )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, nLineStartPos, nLineLength );
			nLine--;
			nSelectEndLine--;
		}
	}
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	return 0;
}

int OnDeleteBlankLineWithWhiteCharacter( struct TabPage *pnodeTabPage )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength , nLineLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( pnodeTabPage == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( pnodeTabPage , & nSelectStartLine , & nSelectEndLine );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		nLineLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINELENGTH, nLine, 0 ) ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos; 
		tr.lpstrText = acLineBuffer; 
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nLineStringLength-1-nDeleteLength] == ' ' || acLineBuffer[nLineStringLength-1-nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength == nLineStringLength )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, nLineStartPos, nLineLength );
			nLine--;
			nSelectEndLine--;
		}
	}
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int OnJoinLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nCurrentLineEndPos ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		nCurrentLineEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINEENDPOSITION, nCurrentLine, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nCurrentLineEndPos, 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CLEAR, 0, 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nCurrentPos, 0 ) ;
	}

	return 0;
}

int OnLowerCaseEdit( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LOWERCASE, 0, 0 );
	return 0;
}

int OnUpperCaseEdit( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_UPPERCASE, 0, 0 );
	return 0;
}

int OnEditEnableAutoAddCloseChar( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bEnableAutoAddCloseChar == FALSE )
	{
		g_stEditUltraMainConfig.bEnableAutoAddCloseChar = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bEnableAutoAddCloseChar = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnEditEnableAutoIdentation( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bEnableAutoIdentation == FALSE )
	{
		g_stEditUltraMainConfig.bEnableAutoIdentation = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bEnableAutoIdentation = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnEditBase64Encoding( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nOutputTextLength ;
	char		*acOutputText = NULL ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = nSelTextLength * 2 ;
	acOutputText = (char*)malloc( nOutputTextLength+1 ) ;
	if( acOutputText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength+1 );
	nret = EVP_EncodeBlock( (unsigned char *)acOutputText , (unsigned char *)acSelText ,(int)nSelTextLength ) ;
	if( nret < 0 )
	{
		::MessageBox(NULL, TEXT("BASE64编码失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acOutputText );
		return -1;
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acOutputText );

	return 0;
}

int OnEditBase64Decoding( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nOutputTextLength ;
	char		*acOutputText = NULL ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = nSelTextLength ;
	acOutputText = (char*)malloc( nOutputTextLength+1 ) ;
	if( acOutputText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength+1 );
	nret = EVP_DecodeBlock( (unsigned char *)acOutputText , (unsigned char *)acSelText , (int)nSelTextLength ) ;
	if( nret < 0 )
	{
		::MessageBox(NULL, TEXT("BASE64解码失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acOutputText );
		return -1;
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acOutputText );

	return 0;
}

int OnEditMd5( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ MD5_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ MD5_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	MD5( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEditSha1( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ SHA_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ SHA_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	SHA1( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEditSha256( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ SHA256_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ SHA256_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	SHA256( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEdit3DesCbcEncrypto( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	long		nOutputTextLength ;
	char		*acOutputText = NULL ;
	char		*acOutputTextExp = NULL ;
	char		key[ 24 + 1 ] ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 8 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = (long)nSelTextLength ;
	acOutputText = (char*)malloc( nOutputTextLength + 1 ) ;
	if( acOutputText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength + 1 );

	acOutputTextExp = (char*)malloc( nOutputTextLength * 2 + 1 ) ;
	if( acOutputTextExp == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acOutputTextExp , 0x00 , nOutputTextLength * 2 + 1 );

	while(1)
	{
		memset( key , 0x00 , sizeof(key) );
		nret = InputBox( g_hwndMainWindow , "请输入加密密钥：" , "输入窗口" , 0 , key , sizeof(key)-1 ) ;
		if( nret == IDOK )
		{
			break;
		}
		else
		{
			free( acSelText );
			free( acOutputText );
			free( acOutputTextExp );
			return 0;
		}
	}

	Encrypt_3DES_CBC_192bits( (unsigned char *)key , (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText , & nOutputTextLength , NULL );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)nOutputTextLength , acOutputTextExp );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );
	free( acOutputText );
	free( acOutputTextExp );

	return 0;
}

int OnEdit3DesCbcDecrypto( struct TabPage *pnodeTabPage )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nInputTextLength ;
	char		*acInputText = NULL ;
	long		nOutputTextLength ;
	char		*acOutputText = NULL ;
	char		key[ 24 + 1 ] ;
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 8 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nInputTextLength = nSelTextLength / 2 ;
	acInputText = (char*)malloc( nInputTextLength + 1 ) ;
	if( acInputText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acInputText , 0x00 , nInputTextLength + 1 );
	HexFold( acSelText , (int)nSelTextLength , acInputText );

	nOutputTextLength = (long)nSelTextLength ;
	acOutputText = (char*)malloc( nInputTextLength + 1 ) ;
	if( acOutputText == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acInputText );
		return -1;
	}
	memset( acOutputText , 0x00 , nInputTextLength + 1 );

	while(1)
	{
		memset( key , 0x00 , sizeof(key) );
		nret = InputBox( g_hwndMainWindow , "请输入解密密钥：" , "输入窗口" , 0 , key , sizeof(key)-1 ) ;
		if( nret == IDOK )
		{
			break;
		}
		else
		{
			free( acSelText );
			free( acInputText );
			free( acOutputText );
			return 0;
		}
	}

	Decrypt_3DES_CBC_192bits( (unsigned char *)key , (unsigned char *)acInputText , (int)nInputTextLength , (unsigned char *)acOutputText , & nOutputTextLength , NULL );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acInputText );
	free( acOutputText );

	return 0;
}

