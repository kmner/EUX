#ifndef _H_EDITULTRA_SOURCECODE_
#define _H_EDITULTRA_SOURCECODE_

#include "framework.h"

int OnViewFileTree( struct TabPage *pnodeTabPage );

int OnViewSwitchFileType( int nFileTypeIndex );

int OnViewSwitchStyleTheme( int nWindowThemeIndex );
int OnViewModifyThemeStyle();
int OnViewCopyNewThemeStyle();

int OnViewHexEditMode( struct TabPage *pnodeTabPage );
int AdujstPositionOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg );
int AdujstKeyOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg );
int AdujstCharOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg );
int SyncDataOnHexEditMode( struct TabPage *pnodeTabPage );
unsigned char *StrdupHexEditTextFold( struct TabPage *pnodeTabPage , size_t *pnTextLength );

int OnViewEnableWindowsVisualStyles();

int OnViewTabWidth( struct TabPage *pnodeTabPage );
int OnViewOnKeydownTabConvertSpaces( struct TabPage *pnodeTabPage );

int OnViewWrapLineMode( struct TabPage *pnodeTabPage );

int OnViewLineNumberVisiable( struct TabPage *pnodeTabPage );
int OnViewBookmarkVisiable( struct TabPage *pnodeTabPage );

int OnViewWhiteSpaceVisiable( struct TabPage *pnodeTabPage );
int OnViewNewLineVisiable( struct TabPage *pnodeTabPage );
int OnViewIndentationGuidesVisiable( struct TabPage *pnodeTabPage );

int OnViewZoomOut( struct TabPage *pnodeTabPage );
int OnViewZoomIn( struct TabPage *pnodeTabPage );
int OnViewZoomReset( struct TabPage *pnodeTabPage );

int OnEditorTextSelected( struct TabPage *pnodeTabPage );

#endif
