#ifndef _H_EDITULTRA_ENV_
#define _H_EDITULTRA_ENV_

#include "framework.h"

extern BOOL g_bIsEnvFilePopupMenuSelected ;
extern BOOL g_bIsEnvDirectoryPopupMenuSelected ;

int OnEnvFilePopupMenu();
int OnEnvDirectoryPopupMenu();

int OnEnvSetProcessFileCommand();
int OnEnvExecuteProcessFileCommand( struct TabPage *pnodeTabPage );
int OnEnvSetProcessTextCommand();
int OnEnvExecuteProcessTextCommand( struct TabPage *pnodeTabPage );

#endif
